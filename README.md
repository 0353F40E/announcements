# Bitcoin Cash Node announcements

This repository contains a list of official public announcements and
reports released by the project. This is considered to be the canonical
source for such official project announcements.

## Structure of contents

Please refer to the contained files which should be named in the format

    YYYY-mm-dd_<some English description with underscores instead of whitepace>_<two letter language code>.md

The date prefix of each file indicates the date of initial publication of the
announcement or article.

Note: Older announcements were prefixed with `YYYYmmdd_` but we are moving
to the ISO-8601 standard for new announcements (after 2021-11-24).

The two letter language codes used are as follows:
- EN: English
- ES: Spanish
- JA: Japanese
- KO: Korean
- ZH: Chinese

Each announcement or article is a date-prefixed file in the main folder,
but may reference content from subfolders such as bitmap images from `img/`.
Where a report includes other files, these files should be named in a way
that clearly associates them with the report, and where possible their most
easily editable source format should be provided for future maintenance and
translation purposes.

## Drafting content for this repository

Please use the `draft/` folder when drafting announcements for which the
publication date is still unknown.

The draft files can be named with placeholders for the date ranges that
need fixing, eg.

    2021-12-xx_technical_report_on_uptake_of_new_feature_XYZ.md

The relocation to the main folder and setting the correct date of publication
(both in the content and in the file naming) will be made as part
of the merge request process after the content has been approved.

## Want to sign an announcement to express your approval?

You can use the git hash of the commit of the version you approve, as part of your signing 'message'.
This strongly identifies the contents that you're signing.

You can alternatively download the raw text, and sign a digest of that.

Ways to sign(al) your support:

*  Bitcoin signing
*  PGP / GPG
*  keybase sign
*  [memo.cash](https://memo.cash) or [Member](https://memberapp.github.io/) posts

Please help us to improve by raising issues if you spot something wrong.

Thanks,

\- The Bitcoin Cash Node team
